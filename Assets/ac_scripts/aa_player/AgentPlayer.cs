﻿using UnityEngine;
using System.Collections;

public class AgentPlayer : Agent
{

    public override void Update()
    {
        velocity.x = Input.GetAxis("Horizontal");
        velocity.z = Input.GetAxis("Vertical");
        velocity *= (VELOCITY_MAG_P*Time.deltaTime);
       // velocity *= VELOCITY_MAG_P;

        Vector3 translation = velocity * Time.deltaTime;
        transform.Translate(translation, Space.World);
        transform.LookAt(transform.position + velocity);
         
    }
}
