using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Agent : MonoBehaviour
{

    public float VELOCITY_MAG_P;
    public float ACCELERATION_MAG_E;
    public float SPEED_MAG_E;
    public float orientation;
    public float rotation;
    public Vector3 velocity;
    [SerializeField]
    protected Steering steering;
    [SerializeField]
    bool enemy;
    

    [SerializeField]
    float veocityMag = 0.0f;

    void Start()
    {
        velocity = Vector3.zero;
        steering = new Steering();
    }
    public void SetSteering(Steering steering)
    {
        this.steering = steering;
    }

    public virtual void Update()
    {
        Vector3 displacement = velocity * Time.deltaTime;
      //  orientation += rotation * Time.deltaTime;
        // we need to limit the orientation values
        // to be in the range (0 � 360)
        /*
        if (orientation < 0.0f)
            orientation += 360.0f;
        else if (orientation > 360.0f)
            orientation -= 360.0f;*/
       transform.Translate(displacement, Space.World);
     //   transform.rotation = new Quaternion();
     //   transform.Rotate(Vector3.up, orientation);
    }

    public virtual void LateUpdate ()
    {

        /*
        veocityMag = velocity.magnitude;


        if (velocity.magnitude > VELOCITY_MAG_P)
        {
            velocity.Normalize();
            velocity = velocity * VELOCITY_MAG_P;
        } 

        Debug.DrawRay(this.transform.position, velocity, Color.red);
        */

       // velocity += steering.linear * Time.deltaTime;
       // rotation += steering.angular * Time.deltaTime;


       if(enemy)
        {

          //  velocity = steering.linear;
            velocity += steering.linear * Time.deltaTime;
            Debug.DrawRay(this.transform.position, velocity, Color.blue);
            // velocity += steering.linear * Time.deltaTime;
            // rotation += steering.angular * Time.deltaTime;

           

            if (velocity.magnitude > SPEED_MAG_E)
            {
                velocity.Normalize();
                velocity = velocity * SPEED_MAG_E;
            }

/*
            if (steering.angular == 0.0f)
            {
                rotation = 0.0f;
            }
            if (steering.linear.sqrMagnitude == 0.0f)
            {
                velocity = Vector3.zero;
            }
*/
        }
        else
        {

            
            Debug.DrawRay(this.transform.position, velocity, Color.red);

            if (velocity.magnitude > VELOCITY_MAG_P)
            {
                velocity.Normalize();
                velocity = velocity * VELOCITY_MAG_P;
            }

        }
        steering = new Steering();



    }
   

    
   
     
}
